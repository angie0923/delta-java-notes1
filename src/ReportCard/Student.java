package ReportCard;

import java.util.Arrays;

public class Student {

    private String name;
    private int[] grades;

    // constructor

    public Student(String name, int[] grades) {
        this.name = name;
        this.grades = grades;
    }


    public void addGrade(int grade) {
        System.out.println(grade);
    }

    public double getGradeAverage() {
        int avg = 0;
        int i;
        for (i = 0; i < grades.length; i++) {
            avg += grades[i]/3;
        }

        return avg;
    }


    // GETTERS / SETTERS


    public String getName() {
        return name;
    }

    public int[] getGrades() {
        return grades;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }

    public static void main(String[] args) {
        Student S1 = new Student("THE AJ", new int[]{90, 80, 90});
        Student S2 = new Student("Cobra Dave", new int[]{89, 91, 77});
        Student S3 = new Student("Double A", new int[]{90, 89, 94});
        Student S4 = new Student("JoshuaTree", new int[]{89, 97, 92});

         //Averages
        System.out.println();
        System.out.println("THE AJ average is: " + S1.getGradeAverage());
        System.out.println("Cobra Dave average is: " + S2.getGradeAverage());
        System.out.println("Double A average is: " + S3.getGradeAverage());
        System.out.println("JoshuaTree average is: " + S4.getGradeAverage());
        System.out.println("==========================================");

         //Names
        System.out.println(S1.getName());
        System.out.println(S2.getName());
        System.out.println(S3.getName());
        System.out.println(S4.getName());
        System.out.println("============================================");

         //Grades
        System.out.println(Arrays.toString(S1.getGrades()));
        System.out.println(Arrays.toString(S2.getGrades()));
        System.out.println(Arrays.toString(S3.getGrades()));
        System.out.println(Arrays.toString(S4.getGrades()));
        System.out.println("============================================");

         //addGrade
        S1.addGrade(99);
        System.out.println(Arrays.toString(S1.grades));

    }
}
