import java.util.Random;

public class ServerNameGenerator {
    // * Create two arrays whose elements are strings,
    // one with at least 10 adjectives,

    public static String[] Adjectives = {
            "adorable",
            "brave",
            "condescending",
            "determined",
            "excited",
            "frothy",
            "grumpy",
            "handsome",
            "ideal",
            "hollow",
            "wicked"
    };

    // another with at least 10 nouns.

    public static String[] Nouns = {
            "Aircraft",
            "Arrow",
            "Bones",
            "Boat",
            "Baby",
            "Cabinet",
            "Planet",
            "Car",
            "Monday",
            "Google",
            "Dinosaur",
    };


   // * Create a method that will return a random
    // element from an array of strings.

    public static void printName(){
        Random random = new Random();
//        StringBuilder messageBuilder = new StringBuilder();
        System.out.println(Adjectives[random.nextInt(Adjectives.length)] +"-"+ Nouns [random.nextInt(Nouns.length)]);
//        String message = messageBuilder.toString();
//        return message;

    }

    public static void main(String[] args) {
        printName();

    } // end of psvm

} // end of class
