import java.util.Scanner;

public class Television {
    // Create a Television class. An instance of class Television represents a show. This class has the following three class variables:
    //- title, which is a String representing the title of the show.
    //- year, which is a int representing the year the show FIRST came out.
    //- studio, which is a String representing the studio that made the show.
    //*Make each variable private

    private String title;
    private int year;
    private String studio;

    //Write a constructor for the class Television, which takes a String representing the
    // title of the show, a String representing the studio, and a int representing the year,
    // and set the respective class variables to these value (i.e this keyword)

    // CONSTRUCTOR

    public Television(){}

    public Television(String title, String studio, int year){
        this.title = title;
        this.studio = studio;
        this.year = year;
        System.out.println("\n" + title);
        System.out.println("\n" + studio);
        System.out.println("\n" + year);
    }

    // field/instance method
    /*

    Solution 2:
    public String displayInfo(){
    return String.format("%s came out in %d, and was produced by %s", title, year, studio);
     */

    /*
    GETTER:
    public String getTitle(){
    return title

    SETTER:
    public void setTitle(String title){
    this.title = title;

    GETTER:
    public String getStudio(){
    return studio;

    SETTER:
    public void setStudio(String studio){
    this.studio = studio;

    GETTER:
    public int getYear(){
    return studio;

    SETTER:
    public void setYear(int year){
    this.year = year;

     */

    public  String getShow(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the name of a tv show: ");
        String input;
        input = scanner.nextLine();
        System.out.println("Please enter the production studio: ");
        String input2;
        input2 = scanner.nextLine();
        System.out.println("Please enter the year released: ");
        int input3;
        input3 = scanner.nextInt();
        System.out.println("You entered:\n " +input+" produced by " +input2+" and released in " +input3);
        return input + input2 + input3;
    }

}



