import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class StringMethodsExercise {
    public static void main(String[] args) {

        // int myLength = "Good afternoon, good evening, and good night"
        String myLength = "Good afternoon, good evening, and good night";


        // Write some java code that will display the length of the string

        int stringLength = myLength.length();
        System.out.println("There are " +stringLength+ " characters"); // returns: 44



        // Use the string, but instead of using 'int myLength',
        // use 'String uCase' Write some java code that will
        // display the entire string in all uppercase

        String uCase = "Good afternoon, good evening, and good night";
        System.out.println(uCase.toUpperCase());



        // Use the same code but instead of using the toUpperCase()
        // method, use the toLowerCase() method.Print it out in the
        // console, what do you see?

        String lCase = "Good afternoon, good evening, and good night";
        System.out.println(lCase.toLowerCase());




        // Copy this code into your main methodString firstSubstring
        // = "Hello World".substring(6);Print it out in the console,
        // what do you see?

        String firstSubstring = "Hello World".substring(6);
        System.out.println(firstSubstring); // returns: World






        // Change the argument to 3, print out in the console.
        // What do you get?

        String firstSubstring3 = "Hello World".substring(3);
        System.out.println(firstSubstring3); // returns: lo World




        // Change the argument to 10, print it out in the console.
        // What do you get?

        String firstSubstring10 = "Hello World".substring(10);
        System.out.println(firstSubstring10); // returns: d





        // Copy this code into your main methodString message =
        // "Good evening, how are you?Using the substring() method,
        // make your variable print out "Good evening"

        String message = "Good evening, how are you?";
        System.out.println(message.substring(message.length()));
        System.out.println(message.substring(0,12));  // returns: Good evening









        // Now using the substring() method,
        // try to make your variable print out "how are you?"

        String message1 = "Good evening, how are you?".substring(14);
        System.out.println(message1); // returns: how are you?







        // Create a char variable named 'myChar' and assign the
        // value "San Antonio"Using the charAt() method return the
        // capital 'S' only.Change the argument in the charAt()
        // method to where it returns the capital 'A' only.

        String myChar = "San Antonio";
        System.out.println(myChar.charAt(0)); // returns: S

        String myChar1 = "San Antonio";
        System.out.println(myChar1.charAt(4)); // returns: A






       // Change the argument in the charAt() method to where
        // it returns the FIRST lowercase 'o' only.Create a


        String myChar2 = "San Antonio";
        System.out.println(myChar2.charAt(7)); // returns: first o



        // String variable name 'alpha' and assign all the
        // names in the cohort in ONE string.

        String alpha = "Alyssa, Jose, Victor, Angela";
        System.out.println(alpha);





        // Using the split() method, create another variable
        // named 'splitAlpha' where it displays the list of
        // names separated by a comma.

        String splitAlpha = "Alyssa, Jose, Victor, Angela";
        System.out.println(Arrays.toString(splitAlpha.split("  "))); // returns: [Alyssa, Jose, Victor, Angela]





        // Using concatenation, print out all three string variables that
        // makes a complete sentence.

        String m1 = "Hello, ";
        String m2 = "how are you? ";
        String m3 = "I love Java!";
        System.out.println(m1 + m2 + m3); // returns: Hello, how are you? I love Java!





        // Using concatenation, print out "You scored 89 marks for your
        // test!"
        // *89 should not be typed in the sout, it should be the integer
        // variable name.

        int result = 89;
        System.out.println("you scored " + result + " marks for your test");




        //    STRING BONUS:=========================================================================
        //Create a class named Timmy with a main method for the following exercise.
//                Timmy is a teenager with a short temper, is responses are very limited.
//        Try to have a conversation with him.
//                - If you ask him a question (an input that ends with a question mark), he will answer 'Sure'
//                - If you yell at him (an input that ends with an exclamation mark), he will answers 'Yo, chill bruh'
//                - If you address him without actually saying anything (empty input), he will say 'Fine, be that way!'
//                - Anything else you say, he'll answer 'Yeah... sure'
//        Write a java program so that a user can TRY to have a conversation with Timmy


        Scanner scanner = new Scanner(System.in);

        System.out.println("What will you say to Timmy?");

        String userResponse = scanner.nextLine();

        if (userResponse.endsWith("?")){
            System.out.println("Sure");
        }
        else if (userResponse.endsWith("!")){
            System.out.println("Yo, chill bruh");
        }
        else if (userResponse.isEmpty()){
            System.out.println("Fine, be that way!");
        }
        else {
            System.out.println("Yeah.....sure");
        }



    }
}























































































