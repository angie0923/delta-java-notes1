package ReportCard;

import java.util.HashMap;
import java.util.Scanner;

public class GradingApp2 {
    public static void main(String[] args) {
        HashMap<String, Student2> students2 = new HashMap<>();
        students2.put("THE AJ", new Student2("Ashton"));
        students2.put("Cobra Dave", new Student2("David"));
        students2.put("Double A", new Student2("Angela"));
        students2.put("JoshuaTree", new Student2("Josh"));

        students2.get("THE AJ").addGrade(90);
        students2.get("THE AJ").addGrade(90);
        students2.get("THE AJ").addGrade(90);

        students2.get("Cobra Dave").addGrade(90);
        students2.get("Cobra Dave").addGrade(90);
        students2.get("Cobra Dave").addGrade(90);

        students2.get("Double A").addGrade(90);
        students2.get("Double A").addGrade(90);
        students2.get("Double A").addGrade(90);

        students2.get("JoshuaTree").addGrade(90);
        students2.get("JoshuaTree").addGrade(90);
        students2.get("JoshuaTree").addGrade(90);

        System.out.println("Welcome to the student grade app!");
        System.out.println("Here are the gitlab usernames of all the students:\n ");



        do {

            Scanner scanner = new Scanner(System.in);
            // list of usernames
            for (String s: students2.keySet()) {
                System.out.printf("|%s|", s);
                System.out.println();
            }
            System.out.println("Which student do you want to see more information on? \n");
            String userInput = scanner.next();
            if (userInput.equalsIgnoreCase("THE AJ")) {
                System.out.printf("Gitlab Username: %s", userInput);
                System.out.println("Name: ");
                System.out.println(students2.get("THE AJ").getName());
                System.out.println(students2.get("THE AJ").getGradeAverage());

            }else if (userInput.equalsIgnoreCase("Cobra Dave")) {
                System.out.printf("Gitlab Username: %s", userInput);
                System.out.println("Name: ");
                System.out.println(students2.get("Cobra Dave").getName());
                System.out.println(students2.get("Cobra Dave").getGradeAverage());

            }else if (userInput.equalsIgnoreCase("Double A")) {
                System.out.printf("Gitlab Username: %s", userInput);
                System.out.println("Name: ");
                System.out.println(students2.get("Double A").getName());
                System.out.println(students2.get("Double A").getGradeAverage());

            }else if (userInput.equalsIgnoreCase("JoshuaTree")) {
                System.out.printf("Gitlab Username: %s", userInput);
                System.out.println("Name: ");
                System.out.println(students2.get("JoshuaTree").getName());
                System.out.println(students2.get("JoshuaTree").getGradeAverage());
            }


        }while (true);



    }

}
