package ReportCard;

import java.util.ArrayList;

public class Student2 {

    private String name;
    private ArrayList<Integer> grades;

    // constructor
    public Student2(String name) {
        this.name = name;
        this.grades = new ArrayList<>();
    }

    // returns student name
    public String getName() {
        return name;
    }

    // adds a grade to grades prop
    public void addGrade(int grade) {
        grades.add(grade);
    }

    // get grade avg method returns average of the students
    public double getGradeAverage() {
        int total = 0;
        for (int grade: this.grades) {
            total += grade;
        }
        return (double) total/grades.size();
    }

    // for testing
    public static void main(String[] args) {

        Student2 s1 = new Student2("Ashton");

       s1.addGrade(90);
       s1.addGrade(100);
       System.out.println(s1.getGradeAverage());


    }











}
