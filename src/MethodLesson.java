public class MethodLesson {
    public static void main(String[] args) {
        // this main method can be seen separate from this MethodLesson class,
        // so we need to instantiate all variables / methods, that we want to use

        // lets add numbers

        int sum = addNumbers(2, 3);
        System.out.println("Using our static method:");
        System.out.println(sum);

        // or you can also do:

        System.out.println(addNumbers(2, 3));

        // call our tenureMessage()
        System.out.println(tenureMessage("Stephen", "Java", 3));
        System.out.println(tenureMessage("Angular"));

        sayHello();// returns: Hello World
        System.out.println();
        sayHello(5); // Hello World Hello World Hello World Hello World Hello World Hello World
        System.out.println();
        sayHello("Delta"); // returns: Hello delta
        System.out.println();
        sayHello("Howdy", "Zion"); // Howdy Zion



    } // end of psvm

    // Method structure
    public static int addNumbers(int num1, int num2){
        return num1 + num2;
    }

    public static int subtractNumbers(int num1, int num2){
        return num1 - num2; // you can duplicate parameter names, just not variable names
    }

    public void greetings(){
        // void methods can be static or non-static - main point is that they will NOT have to return anything
        System.out.println("Hello, good day!");

    }

    // start of tenureMessage method
    public static String tenureMessage(String name, String progLang, int numYears){
        return name + " has been coding " +progLang+ " for " + numYears + "years";
        // returns: "X has been coding Y for Z years
    }

    // ********************* METHOD OVERLOADING *********************************


    public static String tenureMessage(String progLang){
        return "Somebody is good at " +progLang;
    }


    // version 1 sayHello
    public static void sayHello(int times){
        for (int i = 0; i < times; i ++){
            sayHello(); // calling sayHello()
        }
    }

    // version 2 sayHello
    public static void sayHello(){
        sayHello("Hello", "World");  // calling sayHello(greeting, name);
    }

    // version 3 sayHello
    public static void sayHello(String greeting, String name){
        System.out.printf("%s %s ", greeting, name); //
    }

    // version 4 sayHello
    public static void sayHello(String name){
        sayHello("Hello", name); // calling sayHello(greeting, taking in the "name" we put in)
    }

}
