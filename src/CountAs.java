import java.util.Scanner;

public class CountAs {
    // Write a Java method to count all the a's in a string.

    public static int countingTheA(String str){
        int counter = 0;

        for (int i = 0; i < str.length(); i++){
            if (str.charAt(i) == 'a' || str.charAt(i) == 'A'){
                counter++;
            }

        }

        return counter;
    }

    public static void main(String[] args) {

        System.out.println(countingTheA("Hello to all the beautiful people"));

//         use scanner class -- makes more user interactive:
        Scanner scanner = new Scanner(System.in);
        System.out.println("=======================");
        System.out.println("Enter a string: ");

        String userInput = scanner.nextLine(); // or you could do .toLowerCase

        System.out.println("You entered " + userInput);
        System.out.println("Number of A's = " + countingTheA(userInput));
        System.out.println("=======================");


    }

}
