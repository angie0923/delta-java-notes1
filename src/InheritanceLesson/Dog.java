package InheritanceLesson;

/*
How do we INHERIT from another class
- use the extend keyword
 */

public class Dog extends Animal {
    // fields for dog class
    private int eyes;
    private int legs;
    private int tail;
    private int teeth;
    private String fur;


    public Dog(String name, int brain, int body, int size, int weight, int eyes, int legs, int tail, int teeth, String fur) {
        super(name, brain, body, size, weight);
        this.eyes = eyes;
        this.legs = legs;
        this.tail = tail;
        this.teeth = teeth;
        this.fur = fur;
    } // used constructor generator

    // METHOD
    private void chew() {
        System.out.println("This dog is chewing their food...");
    }

    // OVERRIDING METHOD - inherits a method or methods but makes it unique for this class.

    @Override
    public void eat() {
        System.out.println("This dog is eating...");
        chew();

        // call the Animal's eat() inside of the dog's eat() - we use the 'super' keyword
        super.eat();

    }

        /*
        super keyword - allows us to access a superclass' methods and constructors within a subclass.
         */




















































































} // end of class

