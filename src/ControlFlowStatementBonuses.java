import java.util.Scanner;

public class ControlFlowStatementBonuses {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

//        Control Flow and Statements Bonuses


//        1. Write a Java program that takes the user to provide a single character from the alphabet.
//        Print "Vowel" or "Consonant", depending on the user input.
//        If the user input is not a letter (a-z, A-Z), or is a string of length > 1, print an error message.
//                Test Data:
//        Input letter: p
//        Expected Output: This is a Consonant


//        2. Write a Java program to input 5 numbers from the user and find their sum and average
//        Test Data:
//        Input 5 numbers: 1 2 3 4 5
//        Expected Output:
//        The sum of 5 number is : 15
//        The average is : 3.0
        int i, n = 0, s = 0;
        double avg;
        {

            System.out.println("Input 5 numbers, please hit enter after each number : ");

        }
        for (i = 0; i < 5; i++) {
            Scanner in = new Scanner(System.in);
            n = in.nextInt();

            s += n;
        }
        avg = s / 5;
        System.out.println("The sum of those numbers is : " + s + "\nThe Average is : " + avg);

        //        3. Write a java program to print an America flag.
        //                Expected Output
        //* * * * * * ==================================
        // * * * * *  ==================================
        //* * * * * * ==================================
        // * * * * *  ==================================
        //* * * * * * ==================================
        // * * * * *  ==================================
        //* * * * * * ==================================
        // * * * * *  ==================================
        //* * * * * * ==================================
        //==============================================
        //==============================================
        //==============================================
        //==============================================
        //==============================================
        //==============================================

        System.out.println("* * * * * * ================================");
        System.out.println(" * * * * *  ================================");
        System.out.println("* * * * * * ================================");
        System.out.println("* * * * * * ================================");
        System.out.println(" * * * * *  ================================");
        System.out.println("* * * * * * ================================");
        System.out.println("* * * * * * ================================");
        System.out.println(" * * * * *  ================================");
        System.out.println("* * * * * * ================================");
        System.out.println("============================================");
        System.out.println("============================================");
        System.out.println("============================================");
        System.out.println("============================================");
        System.out.println("============================================");
        System.out.println("============================================");



    }

}
























