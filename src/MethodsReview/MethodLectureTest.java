package MethodsReview;

public class MethodLectureTest {
    public static void main(String[] args) {
        // create an object of our method lecture class
        // "instantiating an object"
        /*
        ClassName objectName = new ClassName(arguments);
         */

        MethodsLecture m1 = new MethodsLecture();

        System.out.println(m1.printNumber()); // returns 1

        m1.sayHello();

        // access yell() from MethodLecture class and print a string value?
        System.out.println(m1.yell("Hello"));

        // access the yell() method from the MethodLecture class
        // that prints the return statement of the method that does not have any parameters

        System.out.println("MethodLectureTest: " + m1.yell());
    }
}
