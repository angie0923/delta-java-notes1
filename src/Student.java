public class Student {
    // create instances of a student
    // fields
    // assess modifiers - public and private keywords
    public String name;
    public String cohort;

    // private instance variable
    private double grade;

    // constructor - a way for us to create classes or objects of a class - should have the same
    // name as the class.
    public Student(String studentName, String assignedCohort){
        name = studentName;
        cohort = assignedCohort;
    }

    public Student(String studentName){
        name = studentName;
        cohort = "Unassigned to a Cohort";
    }

    public Student(String name, String cohort, double grade){
        // "this" keyword provides us a way to refer to the CURRENT instance
        // - more preferred way
        this.name = name;
        this.cohort = cohort;
        this.grade = grade;
    }

    public String getStudentInfo(){
        return String.format("Student Name: %s\nCohort Assigned: %s\n", name, cohort);

    }

    public String sayHello(){
        return "Hello, I am " +this.name;
    }

    // must use this to access a private class: if not you will get errors when you try to call "grade"
    // then call the method you created: "shareGrade"
    public double shareGrade() {
        return grade;
    }


}
