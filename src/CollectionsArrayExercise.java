import java.util.ArrayList;
import java.util.Collections;

public class CollectionsArrayExercise {
    public static void main(String[] args) {

        // Create a program that will print out an array of integers.
        ArrayList<Integer> myIntegers = new ArrayList<>();
        myIntegers.add(5);
        myIntegers.add(8);
        myIntegers.add(3);
        myIntegers.add(4);
        myIntegers.add(6);
        System.out.println("These are my integers: " + myIntegers);
        //returns: These are my integers: [5, 8, 3, 4, 6]


        // Create a program that will print out an array of strings
        ArrayList<String> myClass = new ArrayList<>();
        myClass.add("Jose");
        myClass.add("Alyssa");
        myClass.add("Victor");
        myClass.add("Angela");
        System.out.println("This is my class: " + myClass);
        // returns: This is my class: [Jose, Alyssa, Victor, Angela]


        // Create a program that iterates through all elements in an array.
        // The array should hold the names of everyone in class
        for (String c : myClass){
            System.out.println(c);
        }
        // returns: Jose
        //Alyssa
        //Victor
        //Angela

        // OR:
        for (int i = 0; i < myClass.size(); i++){
            System.out.println(myClass.get(i));
        }
        // returns: Jose
        //Alyssa
        //Victor
        //Angela


        // Create a program to insert elements into the array list.
        // This element should be added on the first element and last.
        // You can use a previous array or create a new one.
        myIntegers.add(0,1);
        System.out.println("My new integer array: " + myIntegers);
        // returns: My new integer array: [1, 5, 8, 3, 4, 6]

        myIntegers.add(9);
        System.out.println(myIntegers);
        // returns: [1, 5, 8, 3, 4, 6, 9]

        // Create a program to remove the third element from an array list.
        // You can create a new array or use a previous one.
        myIntegers.remove(3);
        System.out.println(myIntegers);
        // returns: [1, 5, 8, 4, 6, 9]

        // Create an array of Dog breeds.
        // Create a program that will sort the array list.
        ArrayList<String> dogBreeds = new ArrayList<>();
        dogBreeds.add("Cane Corso");
        dogBreeds.add("Pit Bull");
        dogBreeds.add("Great Dane");
        dogBreeds.add("German Shepherd");
        dogBreeds.add("Rottweiler");
        Collections.sort(dogBreeds);
        System.out.println(dogBreeds);
        // returns: [Cane Corso, German Shepherd, Great Dane, Pit Bull, Rottweiler]


        // Create an array of cat breeds.

        ArrayList<String> catBreeds = new ArrayList<>();
        catBreeds.add("Siamese");
        catBreeds.add("Maine Coon");
        catBreeds.add("Burmese");
        catBreeds.add("Sphynx");
        catBreeds.add("Ragdoll");

        // Create a program that will search an element in the array List.
        boolean result;
        result = catBreeds.contains("Maine Coon");
        System.out.println(result ? "The list contains Maine Coon" : "The list does not contain Maine Coon");
        // returns: The list contains Maine Coon

        // OR:
        // System.out.println(catBreeds.contains("Siamese") ? "is in the list" : "is not in the list");

        if (catBreeds.contains("Siamese")) {
            System.out.println("It's on the list!");
        }



        // Create a program to remove the third element from an array list.
        // You can create a new array or use a previous one.
        catBreeds.remove(3);
        System.out.println("My new cat list: " + catBreeds);
        // returns: My new cat list: [Siamese, Maine Coon, Burmese, Ragdoll]


        // Now create a program that will reverse the elements in the array.
        Collections.reverse(catBreeds);
        System.out.println(catBreeds);
        // returns: [Ragdoll, Burmese, Maine Coon, Siamese]










    } // end of psvm

}
