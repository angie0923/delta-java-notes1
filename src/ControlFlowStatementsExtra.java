import java.util.Scanner;

public class ControlFlowStatementsExtra {
    public static void main(String[] args) {
         //Control Flow and Statements Extra
        // 1. Write a Java program to get a number from the user and print whether it is positive or negative
       // Test Data:
//        Input number: 39
//        Expected Output: This number is positive
//        int n;
//        Scanner s = new Scanner(System.in);
//        System.out.print("Enter the number you want to check: ");
//        n = s.nextInt();
//        if(n > 0)
//        {
//            System.out.println("The given number "+n+" is Positive");
//        }
//        else if(n < 0)
//        {
//            System.out.println("The given number "+n+" is Negative");
//        }
//        else
//        {
//            System.out.println("The given number "+n+" is neither Positive nor Negative ");
//        }

//        2. Write a Java program that will take three numbers from the user and print the greatest number.
//                Test Data:
//        Input 1st number: 27
//        Input 2nd number: 74
//        Input 3rd number: 83
//        Expected Output: The greatest: 83

//        System.out.print("Input the 1st number: ");
//        int num1 = s.nextInt();
//
//        System.out.print("Input the 2nd number: ");
//        int num2 = s.nextInt();
//
//        System.out.print("Input the 3rd number: ");
//        int num3 = s.nextInt();
//
//
//        if (num1 > num2)
//            if (num1 > num3)
//                System.out.println("The greatest: " + num1);
//
//        if (num2 > num1)
//            if (num2 > num3)
//                System.out.println("The greatest: " + num2);
//
//        if (num3 > num1)
//            if (num3 > num2)
//                System.out.println("The greatest: " + num3);


//        3. Write a Java program that takes a number from the user and displays the corresponding day of the week (starting with Sunday for 1)
//        Test Data:
//        Input number: 3
//        Expected output:
//        Tuesday

//        Scanner in = new Scanner(System.in);
//        System.out.print("Please enter a number 1-7 to correspond to the days of the week(sunday-saturday): ");
//        int day = in.nextInt();
//
//        System.out.println(getDayName(day));
//    }
//
    // Get the name for the Week
//    public static String getDayName(int day) {
//        String dayName = "";
//        switch (day) {
//            case 1: dayName = "Sunday"; break;
//            case 2: dayName = "Monday"; break;
//            case 3: dayName = "Tuesday"; break;
//            case 4: dayName = "Wednesday"; break;
//            case 5: dayName = "Thursday"; break;
//            case 6: dayName = "Friday"; break;
//            case 7: dayName = "Saturday"; break;
//            default:dayName = "Invalid day range";
//        }

//        return dayName;


//        4. Write a Java program to find the number of days in a month (Not for a leap year)
//        Test Data:
//        Input a month number: 10
//        Expected Output:
//        October has 31 days

//    Scanner s = new Scanner(System.in);

//    int number_Of_DaysInMonth = 0;
//        String MonthOfName = "Unknown";
//        System.out.print("Input a month number: ");
//        int month = s.nextInt();
//        switch (month) {
//            case 1:
//                MonthOfName = "January";
//                number_Of_DaysInMonth = 31;
//                break;
//            case 2:
//                MonthOfName = "February";
//                number_Of_DaysInMonth = 28;
//                break;
//            case 3:
//                MonthOfName = "March";
//                number_Of_DaysInMonth = 31;
//                break;
//            case 4:
//                MonthOfName = "April";
//                number_Of_DaysInMonth = 30;
//                break;
//            case 5:
//                MonthOfName = "May";
//                number_Of_DaysInMonth = 31;
//                break;
//            case 6:
//                MonthOfName = "June";
//                number_Of_DaysInMonth = 30;
//                break;
//            case 7:
//                MonthOfName = "July";
//                number_Of_DaysInMonth = 31;
//                break;
//            case 8:
//                MonthOfName = "August";
//                number_Of_DaysInMonth = 31;
//                break;
//            case 9:
//                MonthOfName = "September";
//                number_Of_DaysInMonth = 30;
//                break;
//            case 10:
//                MonthOfName = "October";
//                number_Of_DaysInMonth = 31;
//                break;
//            case 11:
//                MonthOfName = "November";
//                number_Of_DaysInMonth = 30;
//                break;
//            case 12:
//                MonthOfName = "December";
//                number_Of_DaysInMonth = 31;
//        }
//        System.out.print(MonthOfName + "  has " + number_Of_DaysInMonth + " days\n");



//        5. Write a Java program that accepts three numbers and prints "All numbers are equal"
//        if all three numbers are the same, "All numbers are different"
//        if all three numbers are different, and "Neither all are equal or different" otherwise.
//                Test Data:
//        Input 1st number: 2564
//        Input 2nd number: 3526
//        Input 3rd number: 2456
//        Expected Output:
//        "All numbers are different";
//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("Input first number: ");
//        int a = scanner.nextInt();
//        System.out.println("Input second number: ");
//        int b = scanner.nextInt();
//        System.out.println("Input third number: ");
//        int c = scanner.nextInt();
//
//        if (a == b && a == c)
//        {
//            System.out.println("All numbers are equal");
//        }
//        else if ((a == b) || (a == c) || (c == b))
//        {
//            System.out.println("Neither all are equal or different");
//        }
//        else
//        {
//            System.out.println("All numbers are different");
//        }
//
    }}



























