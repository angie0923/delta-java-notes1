package AbstractAndInterface;
/*
ABSTRACT CLASS - a special type of class that is created strictly to be a 'base class'
for other classes to derive from
- can NOT be instantiated ( can't create an object from it )
- may have fields and methods like normal classes
- they have abstract methods
    - Abstract Methods are methods that have no body and MUST be implemented in the derived class
    - only exist in abstract classes
    - doesn't contain a body

  How to make an abstract class?
  - use the 'abstract' keyword inside of the class definition

 */


// this is the class definition
public abstract class Wrestler {

    // method calculate a general wrestler gets paid
    public static void paymentForPerformance(int hours) {
        System.out.println("The Wrestler's pay for tonight: $" + 250 * hours);
    }

    // ABSTRACT METHODS
    abstract public void wrestlerName(); // does not contain a body

    public abstract void themeMusic();

    public abstract void finisherMove();

}
