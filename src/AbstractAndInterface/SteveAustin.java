package AbstractAndInterface;

/*
extends for class (EC)
implements for interface (i for an i)

 */

public class SteveAustin implements WWE {
    @Override
    public void wrestlerName() {
        System.out.println("Stone Cold Steve Austin");
    }

    @Override
    public void themeMusic() {
        System.out.println("Austin 3:16");
    }

    @Override
    public void finisherMove() {
        System.out.println("Stone Cold Stunner");
    }

    @Override
    public void paymentForPerformance(int hours) {
        System.out.println("this WWE Wrestler's pay: $" + 500 * hours);
    }
}
