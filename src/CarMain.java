public class CarMain {
    public static void main(String[] args) {
        // create 3 instances/objects of class Car

        Car car1 = new Car("Honda", "Pilot", 2016);
        Car car2 = new Car("Honda", "Civic", 2017);
        Car car3 = new Car("Honda", "Fit", 2020);

        System.out.println(car1.carInfo());
        System.out.println(car2.carInfo());
        System.out.println(car3.carInfo());


    }
}
