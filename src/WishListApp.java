import java.util.Scanner;

public class WishListApp {
    public static void main(String[] args) {
        WishListFeatures wishList = new WishListFeatures();

        do {
            System.out.println("Please make a selection from the list below: \n");

            System.out.println(" 1 - add an item to your wishlist\n");

            System.out.println(" 2 - display your wishlist and total number of items\n");

            System.out.println(" 3 - exit");

            System.out.println();

            Scanner scanner = new Scanner(System.in);
            int userInput = scanner.nextInt();

            if (userInput == 1) {
                wishList.addingItem();
            } else if (userInput == 2) {
                System.out.println("Your wishlist items displayed here: ");
                wishList.displayList();
            } else if (userInput == 3) {
                break;
            }

        } while (true);

    }

}
