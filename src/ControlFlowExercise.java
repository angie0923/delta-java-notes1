import java.util.Scanner;

public class ControlFlowExercise {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

//        int i = 9;
//        while (i <= 23){
//            System.out.printf("%s ",i);
//            i++;
//        }

//        int a = 0;
//        do {
//            System.out.println(a);
//            a = a + 2;
//
//        }while (a <= 100);


//        int y = 100;
//        do{
//            System.out.println(y);
//            y = y - 5;
//        }while (y >= -10);

//        long z = 2;
//        do {
//            System.out.println(z);
//            z *= z;
//        }while (z < 1000000);


        //OR

//        int num = 2;
//        do {
//            System.out.println(num);
//            num = (int)Math.pow(num, 2);
//        }while (num < 1000000);
//        String choice;
//
//        do {
//
//
//
//        System.out.print("What number would you like to go up to? ");
//        int userInt = scanner.nextInt();
//        System.out.println("");
//        System.out.println("Here is your table!");
//        System.out.println("");
//        System.out.println("number | squared | cubed");
//        System.out.println("------ | ------- | -----");
//
//        for (int x = 1; x <= userInt; x++) {
//            System.out.println(x + "      |      " + (x * x) +   "   |" + "  " + (x * x * x));
//                 OR
//            System.out.printf("%-6d | %-6d | %-6d%n", num1, num2, num3);
//        }
//        }while (choice.equalsIgnoreCase("y"));


        //Convert given numbers into letter grades.
        //welcome the user
        System.out.println("Welcome to the Letter Grade Conversion");
        System.out.println(); //print a blank line

        //perform conversions until choice is value other than "y" or "Y"
        String choice = "y";
        while (choice.equalsIgnoreCase("y")) {
            //get input from the user
            System.out.print("Enter Numeric Grade:\t\t");
            double score = scanner.nextDouble();
            //convert user numeric grade into letter grade
            char myGrade = 'F';
            if (score >= 88 && score <= 100)
                myGrade = 'A';
            else if (score >= 80 && score<=87)
                myGrade = 'B';
            else if (score >= 69 && score<=79)
                myGrade = 'C';
            else if (score >= 60 && score<=68)
                myGrade = 'D';
            // display conversion result
            String message = "Equivalent Letter Grade:\t" + myGrade;
            System.out.println(message);

            // if user wants to continue
            System.out.println("\n continue: (y/n): \t\t");
            choice = scanner.next();
            System.out.println();
        }














    }
}
