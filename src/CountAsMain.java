import java.util.Scanner;

public class CountAsMain {
    public static void main(String[] args) {
       // instantiate the object of the class name

        CountAs count = new CountAs();
        // just like in angular when we referenced our model:
        // character = DC Hero []





        Scanner scanner = new Scanner(System.in);
        System.out.println("=======================");
        System.out.println("Enter a string: ");

        String userInput = scanner.nextLine(); // or you could do .toLowerCase

        System.out.println("You entered " + userInput);
        System.out.println("Number of A's = " + count.countingTheA(userInput));
        System.out.println("=======================");

        CountAs count2 = new CountAs();
        System.out.println(count2.countingTheA("hey you!"));

        CountAs bob = new CountAs();
        System.out.println(bob.countingTheA("hey, what are you doing?"));
    }
}
