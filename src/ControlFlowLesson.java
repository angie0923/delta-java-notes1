import java.util.Locale;
import java.util.Scanner;

public class ControlFlowLesson {
    public static void main(String[] args) {
        // Comparison Operators
        // ==, >, <, >=, <=, !=
//        System.out.println(5 != 2); // true
//        System.out.println(5 >= 5); // true
//        System.out.println(5 <= 5); // true
//        System.out.println(5 > 5); // false


        // Logical Operators
        // &&, ||
//        System.out.println(5 == 6 && 2 > 1 && 3 != 3); // false
//        System.out.println(5 != 6 || 1 < 2 || 3 != 3); // true

        // .equals(), .equalsIgnoreCase()
        Scanner scanner = new Scanner(System.in);
//        System.out.println("Would you like to start? [y/N]");
//        String userInput = scanner.nextLine();

        // **** DON'T WANT TO DO THIS ****
//        boolean confirm = userInput == 'y';

        // DO THIS INSTEAD!!
//        boolean confirm = userInput.equals('y');

//        if (userInput.equalsIgnoreCase("y")){
//            System.out.println("Game has started, good luck!");
//        }
//        else if (userInput.equalsIgnoreCase("n")){
//            System.out.println("You chose to not start the game, good bye");
//        }
//        else {
//            System.out.println("Answer not recognized");
//        }


        // if statement

//        char letter = ';';
//        if (letter == 'y'){
//            System.out.println("The letter was y.  I guess that means Yes.");
//        }
//        else if (letter == 'n'){
//            System.out.println("So you're saying no?");
//        }
//        else {
//            System.out.println("I don't know what that means...");
//        }


        //==================================================================

        // SWITCH STATEMENTS
//        System.out.println("Enter a grade: ");
//        String userInput = scanner.nextLine().toUpperCase();

//        switch (userInput){
//            case "A":
//                System.out.println("Distinction Honors");
//                break;
//            case "B":
//                System.out.println("Grade: B");
//                break;
//            case "C":
//                System.out.println("Grade: C");
//                break;
//            case "D":
//                System.out.println("Grade: D");
//                break;
//            case "F":
//                System.out.println("Fail...");
//                break;
//            default:
//                System.out.println("Invalid Entry");
//        }

        //===========================================================
        // while loop

//        int i = 0;
//        while (i <= 10){
//            System.out.println("i is " + i);
//            i++;
//        }

        //=================================================================
        // do while loop


//        int x = 0;
//        do {
//            System.out.println(x);
//            x++;
//        } while (x < 5);


        //======================================================
        // for loop

//        for (int y = 0; y < 10; y++){
//            System.out.println(y);
//        }

        //====================================================
        // break and continue

        for (int a = 10; a <= 100; a++){

            if (a == 21){
                continue;
            }
            if (a == 89){
                break;
            }

            System.out.println(a);
        }









    }
}
































































