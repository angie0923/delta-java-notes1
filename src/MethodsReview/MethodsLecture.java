package MethodsReview;

import java.util.Locale;

public class MethodsLecture {
    public static void main(String[] args) {
        // call yell() and print in console
        System.out.println(yell("delta"));

        // call yell method that will print out a number in the console?
        System.out.println("MethodLecture: " + yell(5));


    }

    /*
    method definition - public static returnType nameOfMethod([parameters1, parameter2, ....]){
        // the statements of the method
        // a return statement, if a return type is declared
        // a return type can be int, string, boolean.
        }
        "public" is a visibility modifier, it means it is accessible to the other classes
        "static" defines that the method belongs to the class instead of as an instance to it.

     */

    public static int printNumber(){
        return 1;
    }

    public static void sayHello(){
        System.out.println("Hello!");
    }

    public static long printFive(){
        return 5;
    }

    public static String yell(String str){
        return str.toUpperCase() + "!!!";

    }

    // name of method = yell
    // what is the return type?  String
    // what is the name of the parameter?  str

    // method overloading...
    // having the same method name but with different sets of parameters

    public static String yell(){
        return "I don't know what we're yelling about!";
    }

    public static int yell(int num){
        return num;
    }


}
