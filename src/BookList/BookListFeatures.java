package BookList;

import java.util.ArrayList;
import java.util.Scanner;

public class BookListFeatures {

    // empty array list
    ArrayList<Book> booklist = new ArrayList<>();

    // method to add a new book to the booklist
    public void addingBook() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a title: ");
        String titleInput = scanner.nextLine();

        System.out.println("Enter an author: ");
        String authorInput = scanner.nextLine();

        Book newBook = new Book(authorInput, titleInput);

        // how can we add newBook to ArrayList booklist?
        booklist.add(newBook);

        System.out.println("Book has been added successfully to the list!");

        displayList();

    }
    // method to display the booklist:
    public void displayList() {
        for (Book book : booklist) {
            System.out.println(book.getAuthor() + ": " + book.getTitle());
        }
    }



} // end of class
