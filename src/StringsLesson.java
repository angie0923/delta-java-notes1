import java.util.Locale;
import java.util.Scanner;

public class StringsLesson {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Strings are immutable, cannot change the value
        String firstName = "Delta"; // we can take out the value and it will work fine...so String firstName = "";

//        System.out.println("Enter your name: ");
//        firstName = scanner.nextLine();
//        if (firstName.equals("Delta")){
//            System.out.println("firstName is Delta");
//        }
//        else {
//            System.out.println("firstName is not Delta");
//        }

        System.out.println("Enter your email: ");
        String emailInput = scanner.nextLine();
        emailInput = emailInput.replace(' ', '_');
        System.out.println(emailInput); // returns delta_cohort@email.com

        System.out.println(emailInput.length()); // returns number of characters

        System.out.println("index of @ " + emailInput.indexOf("@")); // returns 5 on admin@email.com

        System.out.println("lastIndexOf of n " + emailInput.lastIndexOf("n")); // returns 4 on admin@email.com

        System.out.println("char at " + emailInput.charAt(3)); // returns i on admin@email.com

        for (int x = 0; x < emailInput.length(); x++){
            System.out.println("email.charAt(x) = " + emailInput.charAt(x)); // separates every character
        }

        if (emailInput.equalsIgnoreCase("admin@email.com")){
            System.out.println("You are admin!!");
        }

        // to check if you work for a company
        if (emailInput.toLowerCase().endsWith("codebound.com")){
            System.out.println("Welcome back to work!!"); // will check that whatever we end with matches
        }



    }
}
