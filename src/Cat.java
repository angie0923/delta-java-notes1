public class Cat {
    // instance variables
    // structure of our "cat"
    String name;
    int age;
    String color;
    String breed;

    // instance methods:

    public static void sleep(){
        System.out.println("This cat is sleeping...");
    }

    public static void play(){
        System.out.println("This cat is now playing...");
    }

    public static void eat(){
        System.out.println("This cat is now eating...");
    }

    public String greeting(){
        return String.format("Hello, I am %s", name);
    }

}
