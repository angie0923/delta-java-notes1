package Exceptions;

public class ExceptionsLesson {

    /*
    EXCEPTIONS / ERROR HANDLING
     - allows us to control the flow of a program when an error occurs
     - there are different types of exceptions, when and how to use them, and
       how to create custom exceptions.

    An exception is an unexpected event that occurs during a program's execution.
    - it affects the flow of the program instructions which can cause the program to terminate.


    An exception can occur for many reasons:
     - Invalid user input
     - device failure
     - loss of network connection
     - physical limitations -- disk memory capacity
     - code errors
     - opening an unavailable file


    Try-catch-finally block

    SYNTAX:
    try {
        do something
        }

    catch (type of error exception){
       do something else when error occurs
       }

    finally {
        do something regardless of whether the try or catch condition is met
        }

     */

    public static void main(String[] args) {

        // example of try / catch block

//        try {
//            int divideByZero = 6/2;
//            System.out.println("Code in try block executed successfully!");
//        }
//        catch (ArithmeticException e) {
//            System.out.println("AtrithmeticException: " + e.getMessage());
            // displays the default built-in exception message for ArithmeticException
//        } // returns: Code in try block executed successfully!

//        try {
//            int divideByZero = 5/0;
//            System.out.println("Code in try block executed successfully!");
//        }
//        catch (Exception e) {
//            System.out.println("Exception: " + e.getMessage());
//            // displays the default built-in exception message for ArithmeticException
//        } // returns: Exception: / by zero



        // EXAMPLE of multiple catch blocks:
        class ListOfNumbers {
            public int[] arrayOfNumbers = new int[10];

            public void writeList() {
                try {
                    arrayOfNumbers[10] = Integer.parseInt("11");
                    System.out.println("Try block code executed");
                }
                catch (NumberFormatException e1) {
                    System.out.println("NumberFormatException: " + e1.getMessage());
                }
                catch (IndexOutOfBoundsException e2) {
                    System.out.println("IndexOutOfBoundsException: " + e2.getMessage());
                }
            }
        } // end of ListOfNumbers class




//        try {
//            ListOfNumbers list = new ListOfNumbers();
//            list.writeList();
//        }
//        catch (Exception e) {
//            System.out.println(e.getMessage());
//        }
//        finally {
//            System.out.println("Finally block executed no matter what!");
//        }


        // example of try-catch-finally block
        try {
            System.out.println(5 * 10);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        finally {
            System.out.println("5*10 try-catch-finally block executed!");
        }



    } // end of psvm
}
