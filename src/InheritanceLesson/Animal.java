package InheritanceLesson;

public class Animal {
    // INHERITANCE - one of key concepts in OOP (Object Oriented Programming)
    /*
    - allows us to create a new class from an existing class so that we can reuse the code.

     */

    // FIELDS - variables

    private String name;
    private int brain;
    private int body;
    private int size;
    private int weight;

    // CONSTRUCTOR

    public Animal(String name, int brain, int body, int size, int weight) {
        this.name = name;
        this.brain = brain;
        this.body = body;
        this.size = size;
        this.weight = weight;
    }

    // METHODS

    public void eat() {
        System.out.println("This animal is eating...");
    }

    public void move() {
        System.out.println("This animal is moving...");
    }

    // GETTERS / SETTERS
    /*
     - our variables are declared private, meaning the variables cannot be accessed outside
     this class.
     to access these variables outside of this class we use getters / setters
     */

    public String getName() {
        return name;
    }

    public int getBrain() {
        return brain;
    }

    public int getBody() {
        return body;
    }

    public int getSize() {
        return size;
    }

    public int getWeight() {
        return weight;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBrain(int brain) {
        this.brain = brain;
    }

    public void setBody(int body) {
        this.body = body;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    // GETTER / SETTER METHODS CREATED
    // - now the private fields (variables) can be accessed by other classes

} // end of class
