package Shapes;

public class ShapesMain {
    public static void main(String[] args) {

//        Rectangle box1 = new Rectangle(5, 4);
//        Rectangle box2 = new Rectangle(5, 3);

//        Measurable myShape = new Rectangle(5,4);
        Measurable myShape1 = new Square(5);
        Measurable myShape3 = new Rectangle(7,5);

//        System.out.println(myShape.getArea());
//        System.out.println(myShape.getPerimeter());
        System.out.println(myShape3.getArea());
        System.out.println(myShape3.getPerimeter());
        System.out.println(myShape1.getArea());
        System.out.println(myShape1.getPerimeter());





//        box1.getArea(); // returns 20
//        box1.getPerimeter(); // returns 18

//        box2.getArea(); // returns 25  returns 25 with overriding method
//        box2.getPerimeter(); // returns 20  returns 20 when overriding method


    }
}
