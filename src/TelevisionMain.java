public class TelevisionMain {

    //- Create a TelevisionMain class.
    //- Include your main method in this class.
    //- Create three objects of the Television class
    //- Write a piece of code that will display a message in your console.
    //EX. "Game of Thrones came out in 2011, and was produced by HBO"

    public static void main(String[] args) {
        Television show1 = new Television("Handmaid's Tale", "Hulu", 2018);
        Television show2 = new Television("The Walking Dead", "AMC", 2010);
        Television show3 = new Television("The One", "Netflix", 2021);
        Television show4 = new Television();

        show4.getShow();

        /*
        Solution 1:
        System.out.println(show1.getTitle() + "Came out in " + show1.getYear() + " and was produced by " + show1.getStudio()+".");

        Solution 2:
        sout(show2.displayInfo());

        //Bonus: Scanner
        Scanner scanner = new Scanner(System.in);
        sout("Enter the year of the first episode: ");

        int yearInput = Integer.parseInt(scanner.nextLine());
        *** int yearInput = scanner.nextInt();  ** cursor stays there with the number instead of going to the
        next line like the .nextLine() would do
        we can also just do :
        int yearInput = scanner.nextInt();
        scanner.nextLine();  ***

        sout("Enter a TV show title:);
        String showInput = scanner.nextLine();
        sout("Enter the studio: ");
        String studioInput = scanner.nextLine();


        Television show5 = new Television(showInput, studioInput, yearInput);
        sout(show4.displayInfo());

         */


    }
}
