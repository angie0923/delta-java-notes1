package Shapes;
public class Rectangle extends Quadrilateral implements Measurable {

//    protected double length;
//    protected double width;

//Constructor
    public Rectangle(double length, double width) {
        super(length, width);

        this.length = length;
        this.width = width;
    }



    @Override
    public double getArea() {
        return length * width;
    }

    @Override
    public double getPerimeter() {
        return (2 * (length + width));
    }

    @Override
    public void setLength(double length) {

    }

    @Override
    public void setWidth(double width) {

    }




    // methods

//    public void getArea() {
//        System.out.println(this.length * this.width);
//    }
//
//    public void getPerimeter() {
//        System.out.println(2 * (this.length + this.width));
//    }






    // GETTERS / SETTERS


//    public double getLength() {
//        return length;
//    }
//
//    public double getWidth() {
//        return width;
//    }
//
//    public void setLength(int length) {
//        this.length = length;
//    }
//
//    public void setWidth(int width) {
//        this.width = width;
//    }
} // end of class


