import org.w3c.dom.ls.LSOutput;

public class PersonMain {
    public static void main(String[] args) {

        Person p1 = new Person("Jackie");
        Person person1 = new Person("John");
        Person person2 = new Person("John");
//        Person person2 = new Person("Jane");


//        System.out.println(p1.getName());

//        p1.setName();


//        p1.sayHello();

        System.out.println(person1.getName().equals(person2.getName())); //  true
        System.out.println(person1 == person2); // false

//        Person person2 = person1;
        System.out.println(person1 == person2); // true
        System.out.println(person1.getName());
        System.out.println(person2.getName());

        person2.setName();
        System.out.println(person1.getName()); // John
        System.out.println(person2.getName()); // Jane





    }


}


