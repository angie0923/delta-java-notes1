import java.util.Arrays;

public class ArraysLesson2 {

    public static void main(String[] args) {
        // Create an array of numbers
        String[] people = {"Ron", "Fred", "Sally"};
        System.out.println(people); // returns [Ron, Fred, Sally]

        // applying method
        String[] morePeople = addPerson(people, "Kevin");
        System.out.println(Arrays.toString(morePeople)); // returns [Ron, Fred, Sally, Kevin]
    } // end of psvm

    // create a method to add a number to an array
    public static String[] addPerson(String[] peopleArray, String person){
        String[] peopleCopy = Arrays.copyOf(peopleArray, peopleArray.length + 1);

        peopleCopy[peopleCopy.length - 1] = person;
        // peopleCopy[lastIndexOf] = person
        return peopleCopy;
    }



} // end of class
