package Exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

public class SpecificError {
    public static void main(String[] args) {

        // Create an integer variable name choice, and assign 0 as the value.
        int choice = 0;

        // Create a Scanner object, name it input, and pass System.in as an argument.
        Scanner input = new Scanner(System.in);

        int[] numbers = {10, 11, 12, 13, 14, 15};
        System.out.println("Please enter the index of the array: ");

        try {
            choice = input.nextInt();
            System.out.printf("numbers[%d] = %d%n", choice, numbers[choice]);
        }
        catch (ArrayIndexOutOfBoundsException exception) {
            System.out.println("Error: Index is invalid");
        }
        catch (InputMismatchException exception) {
            System.out.println("Error: You did not enter an integer");
        }
        catch (Exception exception) {
            System.out.println(exception.getMessage());
        }

        // 10 returns: Error: Index is invalid
        // Hello returns: Error: You did not enter an integer






    }


}

