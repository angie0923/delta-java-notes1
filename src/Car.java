public class Car {


    public String make;
    public String model;
    public int year;


    public Car(String make, String model, int year){
        this.make = make;
        this.model = model;
        this.year = year;
    }

    public String carInfo(){
        return String.format("Make: %s\nModel: %s\nYear: %s\n", make, model, year  );

    }



}

