public class HelloWorld {
    // main method shortcut -- psvm + enter
    public static void main(String[] args) {
        //sout-- inside main method -- this is like our console.log
        System.out.println("Hello World");
        System.out.println("Welcome to Java");

        // single line comments

        /*
        multi
        line
        comments
         */

        // STRINGS
        // - needs to be in double quotations ""
        // char - character needs to be in single quotations ''
//        System.out.println("this will not work");
//        System.out.println('C');

        // ESCAPE CHARACTERS
        System.out.println("Escape Characters: ");
        System.out.println("First \\");   // gives us a \
        System.out.println("Second \n"); // new line
        System.out.println("Third \t tab character");  // tab button

        // VARIABLES
        /*
        All variables in Java MUST be declared before they are used. can't have duplicates
        Syntax:
        dataType nameOfVariable;

         */

        // Examples
        byte age = 13;
        short myShort = -32768;

        int myInteger;
        myInteger = 18;

        boolean isAdmin;
        isAdmin = false;
        System.out.println(isAdmin);

        /*
        CASTING
        Turning a value of one type into another.
        two types: implicit / explicit casting


         */

        // IMPLICIT EXAMPLE
        int myInt = 900;
        long morePrecise = myInt;
        System.out.println(morePrecise);
        // changed 900 from integer to long

        // EXPLICIT EXAMPLE
        double pi = 3.14159;
        int almostPi = (int) pi;
        System.out.println(almostPi);
        // integers are whole numbers not decimals so we only get 3.  we are calling integer so we get 3

//        int a = 2;
//        int b = 5;
//        System.out.println(a + b); // 7

        int a = 20;
        int b = 2;
    //    a = a/b

        a/=b;


        int z = Integer.MAX_VALUE;
        System.out.println(z); // 2147483647

        int zz = Integer.MIN_VALUE;
        System.out.println(zz); // -2147483648

//        int zzz = 2147483648  // too large but we can :

        int zzz = Integer.MAX_VALUE + 1;
        System.out.println(zzz);




    }




}
