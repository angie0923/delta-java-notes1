package AbstractAndInterface;

public class RickFlair extends Wrestler{
    @Override
    public void wrestlerName() {
        System.out.println("Rick Flair");
    }

    @Override
    public void themeMusic() {
        System.out.println("Wooooo!");
    }

    @Override
    public void finisherMove() {
        System.out.println("Figure Four Leg Lock");
    }
}
