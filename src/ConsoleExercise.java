import java.util.Scanner;

public class ConsoleExercise {
    public static void main(String[] args) {

        // Copy this code into your main method:double pi = 3.14159;
//        double pi = 3.14159;

        // Write some java code that uses the variable pi to output the following:
        // The value of pi is approximately 3.14.
//        System.out.printf("The value of pi is approximately %.2f", pi);


        // Explore the Scanner Class
        // Prompt a user to enter a integer and store that
        Scanner scanner = new Scanner(System.in);

//        System.out.println();

//        System.out.println("Enter an Integer: ");
//        int userInt = scanner.nextInt();
//        System.out.println("Thank you, you entered: " + userInt);

        // value in an int variable using the nextInt method.
        // * What happens if you input something that is not an integer?  you get an error.
//        System.out.println("Enter an Integer: ");
//        Integer userInt1 = scanner.nextInt();
//        System.out.printf("The integer you entered was %d", userInt1);

        // Prompt a user to enter 3 words and store each of them in a separate variable,
        // then display them back, each on a newline.
        // * What happens if you enter less than 3 words?
        // * What happens if you enter more than 3 words?


//        System.out.println("Enter one word: ");
//        String userInput = scanner.nextLine();
//        System.out.println("Thank you, you entered: \n" + userInput);
//
//        System.out.println("Enter second word: ");
//        String userInput2 = scanner.nextLine();
//        System.out.println("Thank you, you entered: \n" + userInput2);
//
//        System.out.println("Enter third word: ");
//        String userInput3 = scanner.nextLine();
//        System.out.println("Thank you, you entered: \n" + userInput3);

        // OR
        /*

        System.out.println("Please enter 3 words, hit enter after each entry");
        String userInput1 = scanner.nextLine();
        String userInput2 = scanner.nextLine();
        String userInput3 = scanner.nextLine();

        System.out.println("you entered: " + userInput1 + "\n" + userInput2 + "/n" + userInput3 );

         */





        // Prompt a user to enter a sentence, then store that sentence in a String variable
        // using the .next method, then display that sentence back to the user.
        // * do you capture all of the words? --

//        System.out.println("Please enter a sentence: ");
//        String userSentence = scanner.next();
//        System.out.println("Thank you, you entered: \n" + userSentence); // only shows first word

//        System.out.println();

        // Rewrite the above example using the .nextLine method. --displays the whole sentence
//        System.out.println("Please enter a sentence: ");
//        String userSentence1 = scanner.nextLine();
//        System.out.println("Thank you, you entered: \n" + userSentence1);



        // Calculate the perimeter and area of Code Bound's classrooms
        // Prompt the user to enter values of length and width of a classroom at Code Bound.




        System.out.println("Please enter the length in feet: ");
        double cLength = scanner.nextDouble();
        System.out.println("Enter the width: ");
        double cWidth = scanner.nextDouble();
        System.out.println("The perimeter is " + (2*cLength + 2*cWidth));
        System.out.println("the area is " + (cWidth * cLength));



        //bonus
        // volume of class room
        System.out.println("Enter the volume: ");
        double h = scanner.nextDouble();
        System.out.println("The volume of the class is " + (h * cLength * cWidth));






















    }
}
