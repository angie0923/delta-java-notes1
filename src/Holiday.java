public class Holiday {
    /*
Create a Java class named Holiday. An object of Holiday represents a holiday during the year.
This class has three instance variables:
- name, which is a String representing the name of the holiday
- day, which is an int representing the day of the month of the holiday
- month, which is a String representing the month the holiday is in
*Make each variable private
*/
    private String name;
    private String month;
    private int day;




/*
Write a constructor for the class Holiday, which takes a String representing
the name, an int representing the day, and a String representing the month
as its arguments/parameters, and sets the class variables to these values.
Write some Java code that creates a Holiday instance/object with the
name "Halloween", with the day 31, and with the month "October"
Print out the following in the console:
"Halloween is a holiday on October 31."
*/

    // CONSTRUCTOR
    public Holiday(){}

    public Holiday(String name, String month, int day){
        this.name = name;
        this.month = month;
        this.day = day;
    }

    public String displayHoliday(){
        return String.format("%s is a holiday on %s %d", name, month, day);
    }
}
