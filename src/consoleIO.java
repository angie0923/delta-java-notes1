import java.util.Scanner;

public class consoleIO {
    public static void main(String[] args) {
        // printing out data in the console

//        String cohort = "Delta";
//        System.out.println(cohort);

//        String greeting = "Bonjour";
//        System.out.println(greeting);

        // println v print
//        System.out.print(cohort);
//        System.out.print(greeting); // gives us DeltaBonjour

//        System.out.println();// gives us an empty line

        // souf / printf / format
//        System.out.printf("%s, %s!%n ", greeting, cohort); // prints value for greeting first
//        System.out.format(" %s, %s!", greeting, cohort);
        // FORMAT SPECIFIERS -- %s

//        double price1 = 23.451;
//        double price2 = 50;
//        double price3 = 5.40;
//        double total = (price1 + price2 + price3);
//        System.out.println(total); // returns 78.851
//        System.out.printf("Your total: $%7.2f%n", total); // space in front of amount --$  78.85
//        System.out.printf("Your total: $%7.3f%n", total); // returns $ 78.851
//        System.out.printf("Your total: $%7f%n", total); // returns $78.851000
        // first number (7) is the amount of characters... second number (2) is the amount of decimal places


        // most commonly used, money amounts...two decimal places
//        System.out.printf("Your total: $%.2f%n", total); // returns $78.85


        // ===========================================================================================
        // SCANNER CLASS - get input from the console.
        Scanner scanner = new Scanner(System.in); // can be scanner, sc, or userInput

        // create a prompt
        System.out.println("Enter your name: "); // prompt user to enter data

        // create a variable that will obtain whatever the user enters
        String userInput = scanner.nextLine(); // obtaining the value of user input

        // display the data that the user entered
        System.out.println("Thank you, you entered: \n" + userInput);



        // SCANNER CLASS TO RETRIEVE A NUMBER

        System.out.println("Enter an Integer: ");
        Integer userInt = scanner.nextInt();
//        System.out.println("The integer you entered was " + userInt);
        System.out.printf("The integer you entered was %d", userInt); // using format specifier

        System.out.println("Enter a Decimal: ");
        double userInt1 = scanner.nextDouble();
        System.out.printf("The number you entered was %.2f", userInt1);








    }

}
