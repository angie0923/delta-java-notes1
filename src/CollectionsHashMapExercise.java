import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class CollectionsHashMapExercise {
    public static void main(String[] args) {

        // Create a program that will append a specified element to the end
        // of a hash map.
        HashMap<String, String> mybands = new HashMap<>();
        mybands.put("Manchester Orchestra", "Bed Head");
        mybands.put("Blu DeTiger", "Vintage");
        mybands.put("Gus Dapperton", "Medicine");
        mybands.put("Joy Wave", "After Coffee");
        mybands.put("The Wombats", "Greek Tragedy");
        System.out.println(mybands);
        // returns: {Gus Dapperton=Medicine, Joy Wave=After Coffee, Manchester Orchestra=Bed Head, Blu de Tiger=Vintage, The Wombats=Greek Tragedy}

        mybands.put("The Band Camino", "Daphne Blue");
        System.out.println("My new band list: " + mybands);
        // return: My new band list: {Gus Dapperton=Medicine, Joy Wave=After Coffee, Manchester Orchestra=Bed Head, The Wombats=Greek Tragedy, Blu DeTiger=Vintage, The Band Camino=Daphne Blue}


        // Create a program that will iterate through all the elements in a hash map
        for (Map.Entry me : mybands.entrySet()){
            System.out.println("Key: " + me.getKey() + " & Value: " +me.getValue());
        } // result: Key: Gus Dapperton & Value: Medicine
        //Key: Joy Wave & Value: After Coffee
        //Key: Manchester Orchestra & Value: Bed Head
        //Key: The Wombats & Value: Greek Tragedy
        //Key: Blu DeTiger & Value: Vintage
        //Key: The Band Camino & Value: Daphne Blue


        // OR:
        mybands.forEach((k,v)->
                System.out.println(k + ": " + (v))
        );

        // OR:
        for (String b : mybands.keySet()) {
            System.out.println(b + ":" + mybands.get(b));
        } // Gus Dapperton:Medicine
        //Joy Wave:After Coffee
        //Manchester Orchestra:Bed Head
        //The Wombats:Greek Tragedy
        //Blu DeTiger:Vintage
        //The Band Camino:Daphne Blue



        // Create a program to test if a hash map is empty or not.
        System.out.println(mybands.isEmpty() ? "HashMap is empty" : "HashMap is not empty");
        // returns: HashMap is not empty

        // Create a program to get the value of a specified key in a map
        String val = mybands.get("Joy Wave");
        System.out.println(val); // returns "After Coffee"

        // Create a program to test if a map contains a mapping for the
        // specified key.

        System.out.println(mybands.containsKey("Manchester Orchestra") ? "True" : "False");
        // returns: True

        //Create a program to test if a map contains a mapping for the specified value.
        System.out.println(mybands.containsValue("Bed Head") ? "True" : "False");
        // returns True


        // Create a program to get a set view of the keys in a map
        Set keyset = mybands.keySet();
        System.out.println(keyset);
        // returns: [Gus Dapperton, Joy Wave, Manchester Orchestra, The Wombats, Blu DeTiger, The Band Camino]

        // Create a program to get a collection view of the VALUES contained in a map.
        System.out.println(mybands.values());
        // [Medicine, After Coffee, Bed Head, Greek Tragedy, Vintage, Daphne Blue]





    }
}
