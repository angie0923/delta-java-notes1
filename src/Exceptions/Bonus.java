package Exceptions;

import java.util.Scanner;

public class Bonus {
    public static void main(String[] args) {
        Bonus bonus = new Bonus();
        System.out.println("Please enter a number: ");
        int userInput = bonus.getHex();
        System.out.println(userInput);
    }
        Scanner scanner = new Scanner(System.in);
        public int getBinary() {
            int output;

            try {
                output = Integer.valueOf(scanner.nextLine(),2);
            }
            catch (NumberFormatException e) {
                System.out.println("Invalid Binary Number!");
                return getBinary();
            }

            return output;
        }

        public int getHex() {
            int output;

            try {
                output = Integer.valueOf(scanner.nextLine(),16);
            }
            catch (NumberFormatException e) {
                System.out.println("Invalid Binary Number!");
                return getBinary();
            }

            return output;
        }

        // OR:



}
