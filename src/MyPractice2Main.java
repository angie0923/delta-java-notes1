import ObjectsReview.Athlete;

import java.util.Arrays;

public class MyPractice2Main {

    public static void main(String[] args) {
        // create an object of our method lecture class
        // instantiating an object
        // ClassName objectName = new ClassName(arguments);

//        MyPractice2 p2 = new MyPractice2();
//
//        System.out.println(p2.printNumber()); // returns 1
//
//        p2.sayHello(); // returns Hello!

        // access yell() from MyPractice2 class and print
        // a string value
//        System.out.println(p2.yell("Hello")); // returns HELLO!!!


        // access yell() from MyPractice2 class that prints
        // the return statement of the method that does not
        // have any parameters
//        System.out.println("MyPractice2: " + p2.yell()); // returns MyPractice2: I don't know what we're yelling about!

        int[] numbers = {3, 1, 5, 2, 4};

        System.out.println(numbers);

        int[] numbers2 = Arrays.copyOf(numbers, numbers.length);
        Arrays.sort(numbers2);
        System.out.println(Arrays.toString(numbers2));

        // add a number
        int[] moreNumbers = addNumber(numbers, 78);
        System.out.println(Arrays.toString(moreNumbers));



    }

    // add a number
    public static int[] addNumber(int[] numbersArray, int number){
        int[] numbersCopy = Arrays.copyOf(numbersArray, numbersArray.length + 1);

        numbersCopy[numbersCopy.length - 1] = number;
        return numbersCopy;
    }


}
