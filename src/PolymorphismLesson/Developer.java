package PolymorphismLesson;

public class Developer {
    public String works() {
        return "I'm coding from the office";
    }

    // 'final' keyword prevents the ScrumMaster class from inheriting the works() method


}
