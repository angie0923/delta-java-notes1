public class WishList {

    private String item;
    private double price;

    // constructor

    public WishList(String item, double price) {
        this.item = item;
        this.price = price;
    }



    //GETTERS / SETTERS


    public String getItem() {
        return item;
    }

    public double getPrice() {
        return price;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
