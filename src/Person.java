public class Person {

    private String name;

    // empty constructor
    public Person() {}

    // getters / setters methods
    // gets value of property
    public String getName(){
        return this.name = name;
    }

    // sets a value to a variable
    public void setName(){
        name = getName();
    }

    public void sayHello(){
        System.out.println("Hello from " + this.name);
    }

    // constructor with one parameter
    public Person(String name){
        this.name = name;
    }

    public String shareName(){
        return name;
    }
}
