import java.util.Scanner;

public class MyPractice {
    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("code inside of curly braces");
//        System.out.println("Code indented");
//        System.out.println("I am a string!");
//        System.out.println('c'); // this is a character
//
//        int idNumber;
//        boolean isAdmin;
//        double currentBalance;
//
//        // variables are initialized when they are assigned a value
//
//        idNumber = 321;
//        isAdmin = false;
//        currentBalance = 1400.21;
//
//
//        // Implicit casting
//        int myInteger = 900;
//        long morePrecise = myInteger;
//        System.out.println(morePrecise);
//
//        // Explicit casting
//        double pi = 3.14159;
//        int almostPi = (int) pi;
//        System.out.println(almostPi); // returns 3
//
//
//        // printing data to the console
//
//        String codeName = "Delta";
//        System.out.println("Hello there, " + codeName);
//        System.out.println("Hello " + codeName);
//        System.out.println("Hello " + codeName);
//
//        // output
//        // Hello there, delta
//        // Hello deltaHello delta
//
//        // printf()
//        String codeName1 = "zion";
//        System.out.printf("Hello %s", codeName1);  // the %s will be replaced with the value of the codeName1 variable
//        // returns Hello zion
//
//        String greeting = "Hello";
//        String codeName2 = "zion";
//        System.out.printf("%s %s", greeting, codeName2);
//        // returns Hello zion
//
//        // using scanner
//        System.out.println("Enter something: ");
//        String userInput = scanner.nextLine();
//        System.out.println("You entered: " + userInput);
//
//
//        // Logical operators
//        System.out.println(5 == 6 && 2>1 && 3!=3);// returns false
//        System.out.println(5 != 6 && 2>1 && 3==3); // true
//        System.out.println(5 == 5 || 3 != 3); // true
//
//
//        System.out.println("Would you like to continue? [y/N]");
//        String userInput1 = scanner.next();
//        boolean confirm = userInput1.equals("y");
//        if (confirm == true){
//            System.out.println("Welcome to the jungle");
//        }
//        else if (userInput1.equals("n")){
//            System.out.println("Welcome to the jungle... you entered a lowercase n");
//        }
//        else {
//            System.out.println("Bye bye bye");
//        }
//
//        //switch case
//
//        int switchCase = 1;
//        switch (switchCase){
//            case 1:
//                System.out.println("Case 1");
//            case 2:
//                System.out.println("Default case");
//                break;
//        }
//
//        // While loop
//
//        int i = 0;
//        while (i < 10){
//            System.out.println("i is " + i);
//            i++;
//        }
//
//        // do while loop
//
//        int x = 0;
//        do{
//            System.out.println("x is " + x);
//        }while (x < 10);
//
//
//        // for loop
//        for (int z = 0; z < 10; z++){
//            System.out.println(z);
//        }



        String[] languages = {
                "html",
                "css",
                "javascript",
                "angular",
                "java"
        };

//        for (int i = 0; i< languages.length; i+=1){
//            System.out.println(languages[i]);
//        }

        for (String language : languages){
            System.out.println(language);
        }

        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        // access the first element in the second row
        System.out.println(matrix[1][0]);

        // access the last element in the first row
        System.out.println(matrix[0][2]);

        //access the first element in the last row
        System.out.println(matrix[2][0]);

        for (int[] row : matrix) {
            System.out.println("+---+---+---+");

            System.out.println("| ");

            for (int n : row) {
                System.out.println(n + " | ");
            }

            System.out.println();
        }

        System.out.println("+---+---+---+");




    }

}
