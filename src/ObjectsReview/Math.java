package ObjectsReview;

public class Math {
    // static property

    public static double pi = 3.14159;

    // static method
    public static int add(int x, int y){
        return x + y;
    }

    public static int multiply(int x, int y){
        return x * y;
    }

}
