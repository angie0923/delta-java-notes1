package BookList;

public class Book {
    private String author;
    private String title;


    // constructor

    public Book(String author, String title) {
        this.author = author;
        this.title = title;
    }



    // GETTERS / SETTERS

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
