package AbstractAndInterface;

public class WrestlerMain {
    public static void main(String[] args) {
        Wrestler wrestler1 = new RickFlair();

        WWE wrestler2 = new SteveAustin();

        wrestler1.wrestlerName(); // returns Rick Flair
        wrestler1.finisherMove(); // returns Figure Four Leg Lock
        wrestler1.paymentForPerformance(5); // returns 1250


        System.out.println();
        System.out.println("==========================================");
        System.out.println();



        wrestler2.wrestlerName(); // Stone Cold Steve Austin
        wrestler2.finisherMove(); // Stone Cold Stunner
        wrestler2.paymentForPerformance(5); // this WWE Wrestler's pay: 2500
    }
}
