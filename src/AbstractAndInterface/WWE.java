package AbstractAndInterface;

public interface WWE {
    /*
    INTERFACE - like abstract class - can NOT be instantiated
        instead, they must be implemented by classes or extended by other interfaces
        *** contain abstract methods only ***
        special case of an abstract class or extension of it
     */

    // abstract methods
    public abstract void wrestlerName();
    public abstract void themeMusic();
    public abstract void finisherMove();
    public abstract void paymentForPerformance(int hours);

}
