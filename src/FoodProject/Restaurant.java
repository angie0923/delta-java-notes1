package FoodProject;

import java.util.Arrays;
import java.util.Scanner;

public class Restaurant {

    // what is a restaurant?
    private String name;
    private String type;

    public Restaurant(){}

    public Restaurant(String n, String t){
        this.name = n;
        this.type = t;
    }

    public static String allRestaurants(){
        Restaurant[] restaurants = RestaurantArray.showAllRestaurants();
        String allRest = "";
        for (Restaurant r : restaurants){
            allRest += r.getName() + " - " + r.getType() + "\n";

        }
        return allRest;
    }

    public static String burgerRestaurants(){
        Restaurant[] restaurants = RestaurantArray.showAllRestaurants();
        String burgerRestaurants = "";
        for (Restaurant r : restaurants){
            if (r.getType().equals("burgers")){
                burgerRestaurants += r.getName() + "\n";
            }
        }
        return burgerRestaurants;
    }


    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }
}
