import java.util.Arrays;

public class ArraysExercise {
    public static void main(String[] args) {
        int[] numbers = {3, 1, 5, 2, 4};
        System.out.println(numbers); // returns [I@e580929

        int[] numbers2 = Arrays.copyOf(numbers, numbers.length);
        Arrays.sort(numbers2);
        System.out.println(Arrays.toString(numbers2)); // returns [1, 2, 3, 4, 5]

        // add a number
        int[] moreNumbers = addNumber(numbers, 9);
        System.out.println(Arrays.toString(moreNumbers)); // returns [3, 1, 5, 2, 4, 9]

        // "person" array
        String[] people = new String[3];
        people[0] = "David";
        people[1] = "Noah";
        people[2] = "Ashton";

        System.out.println(Arrays.toString(people)); // returns [David, Noah, Ashton]
        System.out.println(people[0]); // returns David
        System.out.println(people[1]); // returns Noah
        System.out.println(people[2]); // returns Ashton

        // for loop
//        for (int i = 0; i < people.length; i++){
//            System.out.println(people[i]);
//        }

        // enhanced
//        for (String people : people){
//            System.out.println(people);
//        }

        // add to array
        String[] morePeople = addToArray(people, "Josh");
        System.out.println(Arrays.toString(morePeople));

        // digits
        int[] digits = {1, 2, 3};

        System.out.println("Sum of array is: " + sumOfArray(digits));

        // greetings array
        String[] greetings = {"hello", "goodbye", "goodnight"};
        System.out.println(stringTogether(greetings));

    } // end of psvm



    // add a number
    public static int[] addNumber(int[] numbersArray, int number){
        int[] numbersCopy = Arrays.copyOf(numbersArray, numbersArray.length +1);

        numbersCopy[numbersCopy.length - 1] = number;
        return numbersCopy;
    }

    // addToArray() --
    public static String[] addToArray(String[] peopleArray, String person){
        String[] peopleCopy = Arrays.copyOf(peopleArray, peopleArray.length +1);

        peopleCopy[peopleCopy.length - 1] = person;
        return peopleCopy;
    }

    // sumOfArray
    public static int sumOfArray(int[] digits){
        int sum = 0;
        int i;
        for (i = 0; i < digits.length; i++) {
            sum += digits[i];

        }
        return sum;
    }

    // or
    /*
    public static int sumOfArray(int[] digits){
    return (digit[0] + digit[1] + digit[2]);
    }
     */

    // string together

    public static String stringTogether(String[] strArray) {
        String allStr = "";
        for (String eachStr : strArray) {
            allStr += eachStr;
        }
        return allStr;
    }

//    public static String stringTogether(String[] strs){
//        return strs[0] + strs[1] + strs[2];
//    }

}
