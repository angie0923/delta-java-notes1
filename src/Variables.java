public class Variables {
    public static void main(String[] args) {

//        int favoriteNum = 3;
//        System.out.println(favoriteNum); // 3

//        String myName = "Angela" ;
//        System.out.println(myName); // Angela

//        char myName = "Angela";
//        System.out.println(myName);
        // got error to change to String

//        double myName = 3.14159;
//        System.out.println(myName);
        // got an error: required type is string but requires double, once changed it worked

//        long myNum = (long) 3.14;
//        System.out.println(myNum);
        // got message to initialize the variable
        // once i assigned 3.14 to myNum i got another error saying "double" was provided
        // and asked if i wanted to cast to "long"
        // once casted it worked

//        long myNum = 123;
//        System.out.println(myNum);
        //just shows up as 123 with no L
        // 123 with no L just shows as 123 again

//        long Num = 3.14;
//        System.out.println(Num);
        // long data type stores whole numbers
        // double data type stores fractional numbers so we need a double data type, not a long

//        float myNum = 3.14;

        // error wrong type, we can cast to float, or change to double

//        int x = 10;
//        System.out.println(x++); // returns 10 variable is used first then result calculated value currently is 11
//        System.out.println(x); // this one doesn't work because we are declaring the variable already but
//         returns 11 when fixed  value for x is 11 currently because we added one in previous code
//        System.out.println(++x); // returns 12 increment operator so result is calculated and used first...value for x is 12
//        System.out.println(x); // also returns 12  because we incremented by 1 in the previous code.

//        String class = "Hi"; // this is not allowed "class" is one of the naming conventions.

//        String theNumberEight = "eight";
//        System.out.println(theNumberEight); // returns eight

//        Object o = theNumberEight; // error not recognized as a variable
//        int eight = (int) o;
//        System.out.println(eight);

//        int eight = (int) "eight"; // cannot cast string to int
//        System.out.println(eight);

         int x = 5;
//         x = x + 6;
         x += 6;
        System.out.println(x);


















    }
}
