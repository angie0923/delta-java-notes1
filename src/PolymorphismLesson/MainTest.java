package PolymorphismLesson;
/*
Polymorphism is a feature of inheritance that allows us to treat objects of different subclasses/child classes
that have the SAME superclass/parent class as if they were of the superclass type.

Methods of variables that are defined with a superclass type can accept
objects that are a subclass of that type.

FINAL keyword
 - used to prevent inheritance and / or overriding
 */
public class MainTest {
    public static void main(String[] args) {



    // create an instance of the Developer class
    // ClassName objectName = new ClassName(arg);
    Developer developer1 = new Developer();
    Developer developer2 = new Developer();
    Developer developer3 = new Developer();

//        System.out.println(developer1.works());

        // POLYMORPHISM:
        Developer[] googleCompany = new Developer[5];
        googleCompany[0] = new Developer();
        googleCompany[1] = new ScrumMaster();
        googleCompany[2] = new ScrumMaster();
        googleCompany[3] = new Developer();
        googleCompany[4] = new Developer();

        for (Developer dev : googleCompany) {
            doingWork(dev);
        }

    } // end of psvm



    public static void doingWork(Developer dev) {
        System.out.println(dev.works());
    }
} // end of class
