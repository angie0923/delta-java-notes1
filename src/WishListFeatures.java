import java.util.ArrayList;
import java.util.Scanner;

public class WishListFeatures {

    ArrayList<WishList> item = new ArrayList<>();

    public void addingItem() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter an item: ");
        String itemInput = scanner.nextLine();

        System.out.println("Enter the price: ");
        double priceInput = scanner.nextDouble();

        WishList newItem = new WishList(itemInput, priceInput);
        item.add(newItem);

        System.out.println("Item has been successfully added to your wishlist!");



    }


    public void displayList(){
        if (item.isEmpty()){
            System.out.println("0 items");
        }
        else {
            for (WishList i : item)
                System.out.println(i.getItem() + ": $" + i.getPrice());
            System.out.println(item.size());
        }


    }

}
