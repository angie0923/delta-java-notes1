package CollectionsLesson;

import java.util.ArrayList;
import java.util.HashMap;


public class Collections {
    public static void main(String[] args) {
        // ArrayList and its methods
        // ===================================

        // 1.  Initialize an ArrayList of Strings,  call roasts
        // - Add a light, medium, medium, dark to the array list, one at a time
        ArrayList<String> roasts = new ArrayList<>();
        roasts.add("light"); // similar to a JavaScript .push method
        roasts.add("medium");
        roasts.add("medium");
        roasts.add("dark");

        System.out.println(roasts);  // returns [light, medium, medium, dark]

        // 2.  Check to see if the list contains "dark", and then "espresso"
        boolean result;
        result = roasts.contains("dark");
        System.out.println(result ? "It does contain dark!" : "Does not contain dark...womp");
        // returns It does contain dark!


        result = roasts.contains("espresso");
        System.out.println(result ? "It does contain espresso!" : "Does not contain espresso...womp");
        // returns Does not contain espresso...womp


        // 3.  Find the last index of "medium" in the array

        int index = roasts.lastIndexOf("medium");
        System.out.println("The last index of 'medium' is "+ index);// returns The last index of 'medium' is 2

        // 4. Check if the array list is empty
        if (roasts.isEmpty()) {
            System.out.println("The roasts list is empty");
        }
        else {
            System.out.println("The roasts list is not empty");
        }
        // returns The roasts list is not empty

        // TERNARY OPERATOR
        System.out.println(roasts.isEmpty() ? "List is empty" : "List is NOT empty"); // List is NOT empty




        // 5. Assign the array list an empty ArrayList object, and then check if it is empty
        roasts = new ArrayList<>();
        System.out.println(roasts.isEmpty() ? "Line 53 List is empty" : "Line 53 List is not empty");// returns List is empty



        // 6. Put data back in our roasts ArrayList
        roasts.add("light");
        roasts.add("light");
        roasts.add("medium");
        roasts.add("medium");
        roasts.add("dark");
        roasts.add("espresso");
        System.out.println(roasts); // returns [light, light, medium, medium, dark, espresso]



        // 7.  Remove the espresso roast
        roasts.remove("espresso");
        System.out.println("Roasts now looks like " + roasts); // returns: Roasts now looks like [light, light, medium, medium, dark]


        //  Remove every instance of 'light'
        boolean keepGoing = roasts.contains("light");

        while (keepGoing) {
            if (roasts.contains("light")) {
                roasts.remove("light");
            }
            else {
                keepGoing = false;
            }
        }

        System.out.println("After our 'light' removal, roast: " + roasts); // returns: After our 'light' removal, roast: [medium, medium, dark]


        // 8.  Remove the element at index 2
        roasts.remove(2);
        System.out.println("After removing index 2, roasts = " + roasts); // returns: After removing index 2, roasts = [medium, medium]

        // Create an arraylist of integers
        ArrayList<Integer> myNumbers = new ArrayList<>();
        myNumbers.add(23);
        myNumbers.add(50);
        myNumbers.add(32);
        myNumbers.add(91);

        // edit element
        myNumbers.set(3,100);
        System.out.println(myNumbers); //returns: [23, 50, 32, 100]

        // reordering
        java.util.Collections.sort(myNumbers);
        System.out.println(myNumbers); // returns: [23, 32, 50, 100]

        java.util.Collections.reverse(myNumbers);
        System.out.println(myNumbers); // returns : [100, 50, 32, 23]

        //===========================================================================================================

        // HashMaps and their methods

        // 1. Create a HashMap named 'usernames' that contains:
        // a. first name / String
        // b. username / String

        HashMap<String, String> usernames = new HashMap<>();

        // put some data in the hashmap
        usernames.put("Stephen", "stephen001");
        usernames.put("Karen", "beingakaren");
        usernames.put("Juan", "theonly1");
        usernames.put("Leslie", "sleepy21");
        System.out.println(usernames);
        // returns: {Karen=beingakaren, Juan=theonly1, Leslie=sleepy21, Stephen=stephen001}

        // 2. re-intialize the hashmap using the .clear() method
        usernames.clear();
        System.out.println(usernames); // returns: {}

        // 3. use the .put() to add 'AJ' -> 'fridaynext' back to the map
        usernames.put("AJ", "fridaynext");
        System.out.println(usernames); // returns: {AJ=fridaynext}

        // 4. use the .putIfAbsent() method
        usernames.putIfAbsent("Stephen", "stephenguedea");
        usernames.putIfAbsent("Angela", "angelaalexander");
        System.out.println(usernames); // returns: {AJ=fridaynext, Angela=angelaalexander, Stephen=stephenguedea}

        // 5. What happened with the .putIfAbsent()? Did both items get added?  why/whynot?
        // both items did get added because neither were already there.

        usernames.putIfAbsent("AJ", "allenjustin");
        System.out.println(usernames); // nothing is added because there is already a 'key' of "AJ"

        // 6.  use the .remove() method to remove "Stephen"
        usernames.remove("Stephen");
        System.out.println(usernames); // returns: {AJ=fridaynext, Angela=angelaalexander}

        // 7. use the .replace() method to change AJ's username to ablanco
        usernames.replace("AJ","ablanco");
        System.out.println(usernames); // returns: {AJ=ablanco, Angela=angelaalexander}

        // Change the key for "AJ"(have to completely remove and re-add that element to the hashmap)
        String ajValue = usernames.get("AJ");

        usernames.remove("AJ");

        usernames.put("Allen Justin", ajValue);
        System.out.println("After changing 'AJ' key: " + usernames); // returns: After changing 'AJ' key: {Allen Justin=ablanco, Angela=angelaalexander}

        // 8. Use the .clear() method to clear the map
        // 9. Use the .isEmpty() method to verify that it was cleared
        usernames.clear();
        System.out.println(usernames.isEmpty() ? "HashMap is empty" : "HashMap is not empty"); // returns: HashMap is empty

    } // end of psvm

} // end of class
